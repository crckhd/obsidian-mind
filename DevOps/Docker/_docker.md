> Docker is a set of platform as a service products that use OS-level virtualization to deliver software in packages called containers. The service has both free and premium tiers. The software that hosts the containers is called Docker Engine. It was first started in 2013 and is developed by Docker, Inc.
> — [Wikipedia](https://en.wikipedia.org/wiki/Docker_(software))

1. [[DOCKER! Контейнеры]]
2. [[DOCKER! Команды для Dockerfile]]
3. [[DOCKER! Образы контейнеров]]
4. [[DOCKER! Постоянное хранилище]]
5. [[DOCKER! Мультиконтейнеры (без docker-compose)]]
6. [[DOCKER! Мультиконтейнеры (c docker-compose)]]
7. [[DOCKER! Терминал]]

# Источники
* https://docs.docker.com/get-started
* https://www.techtarget.com/searchitoperations/definition/Docker-image