> When running a container, it uses an isolated filesystem. This custom filesystem is provided by a **container image**. Since the image contains the container’s filesystem, it must contain everything needed to run an application - all dependencies, configuration, scripts, binaries, etc. The image also contains other configuration for the container, such as environment variables, a default command to run, and other metadata.
> — [Docker Docs](https://docs.docker.com/get-started/)

Существует несколько типов образов:
* Базовы образ. Образ созданный с нуля
* Родительский образ. Образ созданный для того чтобы на нем создавали другие образы
* Слои. Образ созданный на основе слоев других образов
* Контейнерные слой. Слой созданный на основе контейнера

