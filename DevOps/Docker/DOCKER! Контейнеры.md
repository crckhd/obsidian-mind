> A container is a sandboxed process on your machine that is isolated from all other processes on the host machine. That isolation leverages [kernel namespaces and cgroups](https://medium.com/@saschagrunert/demystifying-containers-part-i-kernel-space-2c53d6979504), features that have been in Linux for a long time. Docker has worked to make these capabilities approachable and easy to use. To summarize, a container:
> -   is a runnable instance of an image. You can create, start, stop, move, or delete a container using the DockerAPI or CLI.
> -   can be run on local machines, virtual machines or deployed to the cloud.
> -   is portable (can be run on any OS)
> -   Containers are isolated from each other and run their own software, binaries, and configurations
>
> — [Docker Docs](https://docs.docker.com/get-started/)

**Контейнер** - независимый процесс, который запущен в песочнице и изолирован от операционной системы. Обычно контейнеры нужны для того чтобы выполнять определенные задачи. Сами контейнеры базируются на _образах контейнеров_ (Container Image) - [[DOCKER! Образы контейнеров]].

Каждый контейнер создается с помощью `Dockerfile`. Сам файл содержит спецалильный синтаксис Dockerfile, с помощью которого мы конфигурируем сам контейнер.

Выглядит файл примерно так:

```dockerfile
# Указываем какой образ нужно схватить за основу
FROM node:12-alpine 

# Мы можем использовать команды консоли прямо внутри Docker-контейнера
RUN apk add --no-cache python2 g++ make

# Мы можем указать рабочую директорию, в которой нонтейнер будет начинать работать
WORKDIR /app

# Мы можем копировать данные с локального диска на виртуальный диск контейнера
COPY . .

RUN yarn install --production

# RUN - нужен для использования консоли при сборке
# CMD запускается каждый раз как мы стартуем контейнер
CMD ["node", "src/index.js"]

# Мы можем протянуть внешние порты в контейнер для доступа к тем или иным серверам
EXPOSE 3000
```

Все команды которые описаны выше есть в [[DOCKER! Команды для Dockerfile]]

## Сборка и запуск контейнера
Для того чтобы собрать контейнер достаточно просто ввести:

```bash
docker build -t название .
```

После того как мы соберем контейнер - нужно его запустить:

```bash
docker run -dp 3000:3000 getting-started
```

Тут есть 2 флага в команде:
* `-d` — указывает на то, что мы не будем следить за тем что сейчас будет делать контейнер. Контейнер запуститься в фоновом режиме (*detached*).
* `-p` — флаг для прокидывания портов. Первый порт является портом хоста, а второй внутри контейнера.

# Пересборка контейнера
Вполне возможно, что мы обновили контент и хотим снова скопировать его в наш контейнер. При пересборке по шагам выше мы наткнемся на то, что старый контейнер все ещё запущен, а запустить новый на том же порте нельзя, так как порт уже занят..

Для того чтобы пересобрать контейнер для начала нам нужно найти запущен ли старый. Сделать это можно достаточно легко с помощью команды `docker ps`, которая покажет все запущенные процессы докера (контейнеры).

В самом выводе у всех контейнеров будут порты. Наша задача найти нужный контейнер и его порт а затем вписать:

```bash
# Останавливаем наш контейнер
docker stop <id>

# Удаляем его
docker rm <id>
```

