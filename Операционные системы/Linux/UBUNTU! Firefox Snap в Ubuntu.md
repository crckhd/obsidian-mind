#linux

- [How to remove Snap completely without losing Firefox?](https://askubuntu.com/questions/1369159/how-to-remove-snap-completely-without-losing-firefox)
- [How to Remove Firefox Snap & Switch Back Classic Deb in Ubuntu 22.04 - FOSTips](https://fostips.com/ubuntu-21-10-two-firefox-remove-snap/)

Для того чтобы корректно удалить Firefox Snap из Ubuntu 21.10, необходимо проделать следующие манипуляции:

```bash
#!/usr/bin/env bash

# Отключаем Firefox от Snap
sudo snap disable firefox

# Удаляем Firefox
sudo snap remove --purge firefox

# Удаляем Firefox Wrapper из системы
sudo apt remove --autoremove firefox

# Добавляем Firefox-ESR в репозитории
sudo add-apt-repository ppa:mozillateam/ppa

# Устанавливаем Firefox ESR (Extended Support Release)
sudo apt install firefox-esr
```