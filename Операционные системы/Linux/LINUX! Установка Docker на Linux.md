#docker #linux 
> В данном документе описывается порядок действий для Ubuntu. Для других систем алгоритм аналогичный, однако меняются команды.

# Удаление существующих бинарников
Для того чтобы установить Docker с официального сайта для начала нужно удалить все существующие версии:

```bash
sudo apt-get remove docker docker-engine docker.io containerd runc
```

# Скачивание дополнительных утилит
Для того чтобы успешно установить Docker на Ubuntu - нужно установить дополнительные утилиты:

```bash
sudo apt-get update
 sudo apt-get install \
    ca-certificates \
    curl \
    gnupg \
    lsb-release
```

# Добавление ключей GPG

```bash
 sudo mkdir -p /etc/apt/keyrings
 curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg
```

# Добавление кастомного репозитория
Так как дефолтные бинарники, предоставляемые из репозиториев Ubuntu нам *не* подходят — установим новый репозиторий:

```bash
echo \
  "deb [arch=$(dpkg --print-architecture) signed by=/etc/apt/keyrings/docker.gpg]  https://download.docker.com/linux/ubuntu \
  $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
```

# Установка Docker
```bash
sudo apt-get update
sudo apt-get install docker-ce docker-ce-cli containerd.io docker-compose-plugin
```

# Шаги после установки
## Права пользователя
По умолчанию докер может запускать только суперпользователь. Можно все время использовать `sudo`, однако это не всегда удобно.

Можно просто добавить пользователя в группу Docker, для того чтобы решить проблему:

```bash
# Создаем группу docker
sudo groupadd docker

# Добавляем текущего пользователя в группу docker
sudo usermod -aG docker $USER
```


## Запуск процесса докера
По умолчанию докер не включен. Для того чтобы включить его достаточно ввести:

```bash
sudo systemctl start docker.service
sudo systemctl start containerd.service
```

Чтобы докер включался при запуске системы достаточно ввести:

```bash
sudo systemctl enable docker.service
sudo systemctl enable containerd.service
```
