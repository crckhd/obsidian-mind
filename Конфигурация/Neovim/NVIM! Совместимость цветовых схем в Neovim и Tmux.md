#neovim #tmux

Для того чтобы Tmux начал поддерживать 256 цветов нужно ввести данные команды в `~/.tmux.conf`:

```bash
# Tell terminal ouside supports 256 colors
set -g default-terminal "tmux-256color"
set -ag terminal-overrides ",xterm-256color:RGB"
```

Чтобы Neovim начал работать в 256 цветах нужно установить данные команды перед установлением темы:

```lua
-- Установить цвета GUI в терминале
vim.o.termguicolors = true
```
