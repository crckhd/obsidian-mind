> [Neovim](https://neovim.io/) is a fork of [Vim](https://wiki.archlinux.org/title/Vim "Vim") aiming to improve the codebase, allowing for easier implementation of APIs, improved user experience and plugin implementation.
> — [ArchWiki](https://wiki.archlinux.org/title/Neovim)

1. [[NVIM! Настройка Neovim в MacOS]]
2. [[NVIM! Совместимость цветовых схем в Neovim и Tmux]]

