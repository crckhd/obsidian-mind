**Двухсторонним связыванием** называется элемент (инпут), который отображает данные из внутреннего состояния компонента и изменяет их.

Если мы напишем что-то подобное, то мы не сможем поменять текст:

```jsx
import "./styles.css";
import {useState} from 'react';

export default function App() {
  let [input, onChangeInput] = useState('Какой-то текст');
  return (
    <div className="App">
      <input value={input} type="text"/>
    </div>
  );
}
```

Все потому что мы дали `input value={input}`, однако забыли поменять onChange, в результате React при вводе заново рендерит текст в `input` и приложение не отзывается на попытку изменить входные данные.

Чтобы все заработало достаточно просто сделать так:

```jsx
import "./styles.css";
import { useState } from "react";

export default function App() {
  let [input, onChangeInput] = useState("Какой-то текст");
  return (
    <div className="App">
      <input
        value={input}
        onChange={(event) => {
          onChangeInput(event.target.value);
        }}
        type="text"
      />
    </div>
  );
}
```

Тут на каждое изменение мы вызываем метод `onChangeInput`, который меняет значение `input`, который в свою очередь отображается в `value`.