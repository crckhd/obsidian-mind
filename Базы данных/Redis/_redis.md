> **Redis** is an in-memory data structure store, used as a distributed, in-memory key–value database, cache and message broker, with optional durability. Redis supports different kinds of abstract data structures, such as strings, lists, maps, sets, sorted sets, HyperLogLogs, bitmaps, streams, and spatial indices.
> — [Wikipedia](https://en.wikipedia.org/wiki/Redis)

Redis - база данных, которая работает внутри оперативной памяти.

Redis хранит все данные в объектах, где есть ключ и значение. Тут нет таблиц, как в SQL-Based БД.

```mermaid
flowchart TB

	subgraph Пара значений в Redis
		Свойство --- Значение
	end
	
```


# UC использования

* Хранение данных в реальном времени
* Кэширование данных
* Хранение данных о сессии
* Стриминг

# Ссылки
* [[REDIS! Сборка Docker-контейнера с Redis]]
* [[REDIS! Работа с полями]]
* [[REDIS! Время жизни полей (Time To Live)]]
* [[REDIS! Структуры данных]]


# Источники
* https://www.youtube.com/watch?v=jgpVdJB2sKQ&t=418s&ab_channel=WebDevSimplified
* https://redis.io/docs/getting-started/

