> **Linked List** - структура данных, которая нужна для создания списков (деревьем с одной веткой), по которым можно итерироваться

У нас есть голова первого элемента и ссылка на следующий элемент. В конце списка следующий элемент (`next`) ведёт к `null`.

Самая простая реализация:

```typescript
const node = {
	data: '12312',
	next: null
};

node.next = {
	data: '12321',
	next: null
};

console.log(node.next.next === null); // true
```

Сама структура выглядит как вложенный объект:

```typescript
interface ILinkedList {
	data: any,
	next: null | ILinkedList
};

const list: ILinkedList = {
	data: '12312231',
	next: {
		data: '12312',
		next: {
			data: '12312321321',
			next: null
		}
	}
};
```

Мы также можем создать такой список как класс (*почему бы и нет?*):

```typescript
class LinkedList {
	
    private _next: LinkedList | null;

	constructor(public data: any, _next?: LinkedList) {
        this._next = _next ?? null;
	}

	set next(list: LinkedList | null) {
		this._next = list;
	}

    get next(): LinkedList | null {
        return this._next;
    }
}

const list = new LinkedList('1231');
list.next = new LinkedList('123121');
console.log(list);
```

