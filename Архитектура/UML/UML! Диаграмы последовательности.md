#uml 

Диаграммы последовательности показывают процесс выполнения определенного действия. Сами диаграммы бывают двух типов:

* ** Для класса**: Показывают как классы взаимодействуют друг с другом, а также в какой очередности выполняют операции
* **Для системы**: показывает как система взаимодействует с внешними факторами (БД, пользователь)

Диаграммы последовательности с самого начала выглядят как-то так:

![[uml_11.svg]]

В данной диаграмме мы опишем систему создания комнат в приложении, когда пользователь перейдет по ссылке.

Вот как выглядит сообщение, которое послал пользователь:

![[uml_12.svg]]

По задумке пользователь должен отправить сообщение клиенту, а тот должен отправить сообщение серверу и проверить есть ли уже комната с таким ID, если комнаты нет - создается новая комната. Если комната есть - пользователь подключается к уже существующей комнате.

Полностью вся диаграмма будет выглядеть вот так:

![[uml_13.svg]]

Рассмотрим отдельные её части:

![[uml_14.png]]

Сплошная полоска означает действие. Пунктирная полоска же означает то, что объект возвращает какое-то значение.

![[uml_15.png]]

Если в последовательности есть ветвление, то принято добавлять в названия OK и NOT OK, а затем описывать возвращаемое значение. **Важно**, что каждое действие отмечено числом последовательности (порядковый номер действия), а значит что мы должны дожать каждое флоу до конца. Если мы вернули OK, то мы должны довести сначала его до пользователя, а потом уже принятся отрисовывать NOT OK.
