#uml 

# Про что?
В данном разделе раскроем основы про диаграммы классов в UML. Как несложно догадаться данные диаграммы моделируют классы, их поведение, содержание, а также отношения классов😌

# Принципы работы
Базовая структура диаграммы класса выглядит вот так:

```mermaid
classDiagram
    class Animal
        Animal : +int age
        Animal : +String gender
        Animal: +isMammal()
        Animal: +mate()
```

Сверху такого объекта - название. Вторым разделом идут свойства класса, а третьим - его методы. Создадим класс `cache`, в нем создадим свойство `id` и сделаем 2 методы: `getId` и `setId`:

![[uml_2.svg]]

Теперь когда мы понимаем основы можно поговорить о типах данных.

## Тип данных
Тип данных в классе UML обозначается через `: type`, прямо как в TypeScript! Возьмем наш класс cache и дадим каждому свойству типы:
![[uml_3.svg]]

То же самое работает и с методами. Синтасис у них один в один, как в TypeScript😳

## Видимость
Не секрет, что в классах есть видимость. Таким образом мы можем создавать инкапсуляцию методов и свойств внутри класса. Создадим новый класс и назовем его `Person`, попробуем сделать некоторые поля защищенными и приватными:
![[uml_4.svg]]

Мы видим тут много разных идентификаторов, однако нам нужно всего лишь понять следующее:
* **+** - публичные методы/свойства - поля, которые доступны вне класса, внутри класса и в дочерних классах.
* **#** - защищенные методы/свойства - поля, которые доступны внутри класса и в дочерних классах.
* **-** - приватные методы/свойства - поля, которые доступны _только_ внутри класса.

## Отношения
Отношения классов в UML - очень важная тема. С помощью отношений можно указать на наследование, принадлежность и так далее. Рассмотрим типы отношений по очереди🦊

### Наследование
Наследование - это когда один класс образуется от другого и наследует все методы и свойства класса-родителя. Тут легче всего пойти от обобщения и показать классы: `Policeman`  и `Robber`:

![[uml_5.svg]]

Как можно видеть у классов `Policeman` и `Robber` идут стрелки _с пустым наконечником_ к `Person`. Это значит, что у `Policeman` и `Robber` будут все свойства и методы из `Person`,  а также свои свойства и методы внутри дочерних классов.

Класс, от которого наследуются принято называть родительским классом или **супер-классом**

Классы, которые идут от супер-класса называются **дочерними**.

Если мы никогда не будем использовать класс `Person`, а будем от него только наследоваться, то можно сделать данный класс абстрактным. Абстрактные классы в UML выделяются курсивом (*`Person`*) или двойными кавычками (`<<Person>>`).

### Ассоциация
Ассоциация используется тогда, когда между классами нет отношений наследования, но они каким-то образом взаимодействуют друг с другом:

![[uml_6.svg]]

### Агрегация и композиция
**Агрегация** - это взаимотношение двух классов, когда один класс может находиться в ассоциации с другим, а может и не находиться. Например, один грабитель может быть на свободе, а может быть в тюрьме, в данном случае мы используем агрегацию:

![[uml_7.svg]]

**Композиция** - это взаимотношение двух классов, когда один класс должен обязательно быть в ассоциации с другим. Он не может существовать сам посебе. Например полицейский не может существовать без полицейского участка, он обязан там числиться:

![[uml_8.svg]]

### Численные отношения
Когда нам нужно понять сколько классов находятся в отношении с другим классом - мы используем численные отношения.
Например, в полицейском участке всегда должно быть больше чем 1 полицейский, тогда мы используем следующую схему:

![[uml_9.svg]]

Однако грабителей в тюрьме может быть сколько угодно, а может тюрьма пустая, тогда мы используем отношение `0..*`:

![[uml_10.svg]]

Отношения численноести бывают:
* `0..1` - или подкласса нет, или он один
* `0` - подкласса нет
* `1` - один подкласс
* `0..*` - Или подкласса нет, или их бесконечно много (может быть)
* `1..*` - Подкласс всегда 1 и больше
* `*` - любое количество подклассов