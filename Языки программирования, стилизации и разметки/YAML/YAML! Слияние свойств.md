Допустим что у нас есть базовые настройки:

```yaml
default:
	name: Alex
	surname: Mitt
	age: 19
	gender: male
```

Теперь допустим, что у нас есть другой пользователь, который подходит под данные настройки, однако имя у него другое. Мы можем импортировать все свойства из одного объекта в другой при этом поменяв позже определенные значения:

```yaml
default: &default_person
	name: Alex
	surname: Mitt
	age: 19
	gender: male
	

mike:
	<<: *default_person
	name: Mike
```

В JSON это будет выглядеть вот так:

```json
{
	"default": {
		"name": "Alex",
		"surname": "Mitt",
		"age": 19,
		"gender": "male"
	},
	"mike": {
		"name": "Mike",
		"surname": "Mitt",
		"age": 19,
		"gender": "male"
	}
}
```

Тут мы использовали ссылки, которые описаны вот здесь [[YAML! Структуры данных]]

