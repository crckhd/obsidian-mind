В YAML строки сделаны очень удобно. Можно удобно управлять escape-последовательностями прямо из YAML.

# Блочная нотация
Например нам доступна следующая запись в YAML:

```yaml
content:
   Arbitrary free text
   over multiple lines stopping only
   after the indentation changes...
```

Все новые строки тут станут обычными пробелами:
```json
{
	"content": "Arbitrary free text over multiple lines stopping only after the indentation changes..."
}
```

# Нотация с литералами
Если мы используем `|` сразу после двоеточего, то YAML будет ставить \\n после каждого переноса строки:
```yaml
content: |
   Arbitrary free text
   over multiple lines stopping only
   after the indentation changes...
```

В JSON это будет выглядеть вот так:

```json
{
  "content": "Arbitrary free text\nover multiple lines stopping only\nafter the indentation changes...\n"
}
```

---

Тоже самое, что и вверху будет делать `|+`, только тут мы явно указываем, что в конце строки тоже будет `\n`
```yaml
content: |+
   Arbitrary free text
   over multiple lines stopping only
   after the indentation changes...
```

```json
{
  "content": "Arbitrary free text\nover multiple lines stopping only\nafter the indentation changes...\n"
}
```

---

А вот если указать `|-`, то `\n` в конце строки не будет:

```yaml
content: |-
   Arbitrary free text
   over multiple lines stopping only
   after the indentation changes...
```

Обратите внимание на последний `\n` — его нет.
```json
{
  "content": "Arbitrary free text\nover multiple lines stopping only\nafter the indentation changes..."
}
```

# Вложенная нотация
Разница между вложенной нотацией и литеральной в том, что вложенная нотация будет срезать лишние новые строки:

## Пример с литеральной нотацией

```yaml
content: |
   Arbitrary free text
   over multiple lines stopping only
   
   after the indentation changes...
```

В JSON это будет выглядеть вот так:

```json
{
  "content": "Arbitrary free text\nover multiple lines stopping only\n\nafter the indentation changes...\n"
}
```

> Тут нужно обратить внимание на два `\n` перед словом *after*

## Пример с вложенной нотацией

```yaml
content: >
   Arbitrary free text
   over multiple lines stopping only
   
   after the indentation changes...
```

В JSON это выглядит вот так:

```json
{
  "content": "Arbitrary free text\nover multiple lines stopping only\nafter the indentation changes...\n"
}
```

> Обратите внимание, что лишний `\n` ушел.

# Обворачивание с помощью `|`
Допустим что у нас есть строка

```
url: "123"
```

Нам нужно обернуть её в строку. Из двух нижеперечисленных методов - 2 сработает, а 1 отдаст url как ключ:

```yaml
 code: 
    url: "123"

 code2: |-
     url: "123"
```

В JSON это выглядит вот так:

```json
{
  "code": {
    "url": 123
  },
  "code2": "url: 123"
}
```