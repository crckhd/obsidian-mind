> **YAML** is a human-readable data-serialization language. It is commonly used for configuration files and in applications where data is being stored or transmitted.
> -- [Wikipedia](https://en.wikipedia.org/wiki/YAML)

YAML находится в файлах с расширением `.yml`. Каждый файл должен начинаться с `---`, причем в один файл YAML можно запихнуть сразу несколько объектов! Достаточно просто разделить их с помощью `---`.



1. [[YAML! Типы данных]]
2. [[YAML! Структуры данных]]
3. [[YAML! Строки]]
4. [[YAML! Расстановка новых строк]]
5. [[YAML! Слияние свойств]]
6. [[YAML! null в разметке]]

