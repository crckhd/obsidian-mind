# Массивы в YAML
Массивы в YAML выглядят следующим образом:
```yaml
 array:
   - 132
   - 2.434
   - 'abc'
```

Многомерные массивы могут выглядеть следующим образом:
```yaml
 array:
   - [1,2,3]
   - [4,5,6]
   - [7]
```

# Объекты в YAML
Объекты в YAML намного упрощены и имеют обратную совместимость в JSON

```yaml
 first:
   subkey:
     sub_subkey: 1
     sub_subkey_2: 2
   another_subkey:
     else: 'hello!'
```

Запись вверху эквивалентна следующей записи в JSON:

```json
{
	"first": {
		"subkey": {
			"sub_subkey": 1,
    		"sub_subkey_2": 2
		},
		"another_subkey": {
			"else": "hello!"
		}
	}
}
```

# Ссылки
В YAML также есть ссылки, на которые можно ссылаться🗿

```yaml
 values:
   - &ref "Something to reuse"
   - *ref
```


JSON: 
```json
{
	"values": ["Something to reuse", "Something to reuse"]
}
```