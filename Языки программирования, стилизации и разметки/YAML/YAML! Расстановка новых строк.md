Новые строки не ставятся, если написать свойство и значение в одну строку:

```yaml
property: | some string
	with new line
```

Данная нотация превратится в 

```json
{
	"property": "some string with new line\n"
}
```

---

Чтобы это исправить можно писать многострочные строки с новой строки:

```yaml
property: |
	some string
	with new line
```

В JSON это будет выглядеть вот так:

```json
{
	"property": "some string\n with new line\n"
}
```