#css #html

Для того чтобы сделать «притягивание» к блокам на сайте (как это устроено обычно в слайдерах для фото), нужно сделать следующее:

```html
<div class="wrapper">
	<div class="child"></div>
	<div class="child"></div>
	<div class="child"></div>
</div>
```

```css
body {
	height: 100vh;
	/* Делаем так, чтобы на body не было скролла */
	overflow: hidden;
}

.wrapper {
	/* Сам снэп ставим на контейнер */
	scroll-snap-type: y mandatory;

	/* Делаем скролл внутри контейнера */
	overflow: auto;

	/* Для плавного скролла */
	scroll-behaviour: smooth;
}

.child {

	/* Говорим к какой части дочерних блоков скроллить */
	scroll-snap-align: start;
}
```

