#javascript

**Условие:** Есть два промиса, один из которых является обязательным, а другой нет. Нужно сделать так, чтобы коллбэк `ok` сработал единовременно на оба промиса вне зависимости от того, какой статус у необязательного промиса

```tsx
Promise.all([Promise_1, Promise_2.catch(console.error)])
    .then(ok).catch(onError);
```

# В чем суть решения?

`Promise_1` - является обязательным промисом. Если он упадет, то вызовет обработку `onError`.

`Promise_2` - является необязательным промисом. Если он упадет, то then все равно выполнится, т.к. он уже был обработан с помощью catch, который в свою очередь вернет `Promise<void>` и не вызовет общий `catch(onError)`.