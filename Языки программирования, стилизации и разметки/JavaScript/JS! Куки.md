> **Куки** – это небольшие строки данных, которые хранятся непосредственно в браузере. Они являются частью HTTP-протокола, определённого в спецификации [RFC 6265](https://tools.ietf.org/html/rfc6265).
> — [learn.javascript](https://learn.javascript.ru/cookie)

Куки в основном добавляются сервером с помощью `Set-Cookie` заголовка, а сами куки отправляются почти с каждым запросом.

> Одно куки вмещает до 4kb данных, разрешается более 20 куки на сайт (зависит от браузера).

# Куки в браузере
По умолчанию все куки из браузера хранятся в `document.cookie` в виде строки. В самой строке будут данные в формате:

```
свойство=значение;
```

Для того чтобы получить все куки достаточно просто сделать:

```javascript
// Массив с куками
const cookiesList = document.cookie.split('; ');
```

Сама строка в `document.cookie` работает по принципу акссесора - мы можем менять только те значения, которые указываем:

```javascript
// Добавит к уже существующим куки новое поле username=Daniil;
document.cookie = 'username=Daniil';
```

# Специальные значения и ключи
У куки есть несколько специальных свойств:

## path
`path` - указывает путь, по которому куки будут доступны

Установить 	сам `path` можно с помощью `document.cookie`

```js
// Устанавливаем путь, который будет начинаться с index.html и охватывать все дочерние элементы
document.cookie = 'path=/';
```

## domain
`domain`  - указывает домен (_один_), в котором файлы куки будут работать

Тут важно сказать, что если мы явно укажем домен `site.com`, то поддомен тоже `other.site.com` тоже будет работать.

```js
// Устанавливаем домен для cookie
document.cookie = 'samesite.com';
```

Также стоит обратить внимание на то, что по умолчанию ключа `domain` нет в куки, он вычисляется автоматически

## expires
`expires` - удаляет куки по истчению времени заданного в GMT

Важно знать, что куки без max-age или expires удаляются сразу после выключения браузера

Получить, кстати говоря, такое время можно с помощью `date.toUTCString`

```js
document.cookie = 'expires=Tue, 05 Jul 2022 07:31:18 GMT';
```

## max-age
`max-age` - время в секундах сколько будет жить куки.

```js
// Создадим куки, который будет жить 2 часа
// Время указывается в секундах
const MINUTE = 60;
document.cookie = 'max-age=' + MINUTE * 120;
```

## secure
По умолчанию cookie доступны на HTTPS и HTTP. То есть если мы перейдем на сайт `https://mysite.com` и на `http://mysite.com` - куки будут доступны в обоих случаях.

Для того чтобы сделать так, чтобы оверхед к пакетам состоящий из куки отправлялся только с HTTPS, нужно указать параметр `secure`:

```js
document.cookie = 'name=Daniil;secure';
```

# Ссылки
* https://learn.javascript.ru/cookie