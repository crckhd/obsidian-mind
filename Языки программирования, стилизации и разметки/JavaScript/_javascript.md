> JavaScript, often abbreviated JS, is a programming language that is one of the core technologies of the World Wide Web, alongside HTML and CSS. As of 2022, 98% of websites use JavaScript on the client side for web page behavior, often incorporating third-party libraries.
> — [Wikipedia](https://en.wikipedia.org/wiki/JavaScript)


1. [[JS! Обработка промиса с помощью catch в Promise.all]]
2. [[JS! Куки]]
3. [[JS! Request Animation Frame (WRAF)]]
4. [[JS! Session и Local Storage]]
5. [[JS! IDB]]