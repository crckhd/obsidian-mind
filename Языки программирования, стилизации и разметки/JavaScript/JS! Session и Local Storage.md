> Свойство **`localStorage`** позволяет получить доступ к [`Storage`](https://developer.mozilla.org/ru/docs/Web/API/Storage) объекту. 
> `localStorage` аналогично свойству [sessionStorage](https://developer.mozilla.org/en-US/docs/Web/API/Window/sessionStorage). Разница только в том, что свойство `sessionStorage` хранит данные в течение сеанса (до закрытия браузера), в отличие от данных,  находящихся в свойстве `localStorage`, которые не имеют ограничений по времени хранения и могут быть удалены только с помощью JavaScript.
> — [MDN](https://developer.mozilla.org/ru/docs/Web/API/Window/localStorage)

Также как и в куки-файлах [[JS! Куки]], объекты массивы и все остальное в объекте Storage хранится в виде строки, только в отличие от печенья - `Storage` сам умеет преобразовывать данные струкуры данных.

Вот пример такого преобразования:

```js

// Записываем информацию в Local Storage
window.localStorage.setItem('array', [1,2,3])
> undefined

// Достаем информацию из LocalStorage
window.localStorage.getItem('array')
> '1,2,3'
```

Помимо двух основных методов, которые представлены выше у объекта `Storage` также есть методы:
* `clear` - очищает хранилище
* `removeItem(key)` - удаляет ключ и свойство из хранилища

> Стоит знать, что LocalStorage ограничен 5 мегабайтами, также как и Session Storage. Для больших объемов данных рекомендуется использовать другие средства для запоминания состояний между сессиями и в пределах сессий [[JS! IDB]]
